=== HideWPBar ===
Contributors: Io. D
Donate link:
Tags:
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.5
Tested up to: 3.5
Stable tag: 0.1

Hide Admin bar for all users except administrators

== Description ==

Hide Admin bar for all users except administrators

== Installation ==


== Frequently Asked Questions ==


== Screenshots ==


== Changelog ==

= 0.1 =
- Initial Revision
